import 'dart:io';

import 'package:majootestcase/models/user.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DbHelper {
  static DbHelper? _dbHelper;
  static Database? _database;

  DbHelper._createObject();

  factory DbHelper() {
    if (_dbHelper == null) {
      _dbHelper = DbHelper._createObject();
    }
    return _dbHelper!;
  }

  Future<Database> initDb() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = directory.path + '/movies.db';
    var todoDatabase = openDatabase(path, version: 1, onCreate: _createDb);
    return todoDatabase;
  }

  void _createDb(Database db, int version) async {
    await db.execute('''
      CREATE TABLE users (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        username TEXT UNIQUE,
        email TEXT UNIQUE,
        password TEXT
      )
    ''');
  }

  Future<Database> get database async {
    if (_database == null) {
      _database = await initDb();
    }
    return _database!;
  }

  Future<bool> login(String email, String password) async {
    Database db = await this.database;
    var mapList = await db.query('users',
        where: "email = ? AND password = ?",
        whereArgs: [email, password],
        limit: 1);
    return mapList.length > 0;
  }

  Future<int> isExistRow(User data) async {
    Database db = await this.database;
    var mapList = await db.query('users',
        orderBy: 'username',
        where: "username = ? OR email = ?",
        whereArgs: [data.userName, data.email],
        limit: 1);
    return mapList.length;
  }

//create databases
  Future<int> insertUser(User object) async {
    Database db = await this.database;
    int count = await db.insert('users', object.toJson());
    return count;
  }

//update databases
  Future<int> updateUser(User object) async {
    Database db = await this.database;
    int count = await db.update('users', object.toJson(),
        where: 'email=?', whereArgs: [object.email]);
    return count;
  }

//delete databases
  Future<int> deleteUser(int id) async {
    Database db = await this.database;
    int count = await db.delete('users', where: 'id=?', whereArgs: [id]);
    return count;
  }
}
