class Preference {
  static const USER_INFO = "user-info";
}

class Api {
  static const BASE_URL = "https://api.themoviedb.org/3/";
  static const API_KEY = "4614f9c1bb5c0adf107978ea48699ee4";
  static const LOGIN = "/login";
  static const REGISTER = "/register";
}

class Font {}

class ScreenUtilConstants {
  static const width = 320.0;
  static const height = 640.0;
}
