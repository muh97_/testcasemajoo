import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Helper {
  static void showToast(BuildContext context, String msg,
      {Toast? duration,
      ToastGravity? gravity,
      Color? backgroundColor,
      Color? textColor,
      double? fontSize}) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: duration,
        gravity: gravity,
        backgroundColor: backgroundColor,
        textColor: textColor,
        fontSize: fontSize);
  }

  static bool isValidEmail(String email) {
    final pattern = RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
    return (pattern.hasMatch(email));
  }
}
