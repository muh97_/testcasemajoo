import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/ui/home/home_screen.dart';
import 'package:majootestcase/ui/login/login_page.dart';

import 'bloc/auth_bloc/auth_bloc_cubit.dart';
import 'bloc/auth_bloc/auth_bloc_state.dart';
import 'bloc/home_bloc/home_bloc_cubit.dart';
import 'ui/extra/loading.dart';
import 'utils/helper.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: BlocProvider(
        create: (context) => AuthBlocCubit()..fetchHistoryLogin(),
        child: MyHomePageScreen(),
      ),
    );
  }
}

class MyHomePageScreen extends StatelessWidget {
  MyHomePageScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBlocCubit, AuthBlocState>(builder: (_, state) {
      if (state is AuthBlocInitialState || state is AuthBlocLoadingState) {
        return LoadingIndicatorScreen();
      } else if (state is AuthBlocLoginState) {
        return LoginPage();
      } else if (state is AuthBlocLoggedInState) {
        return BlocProvider(
          create: (context) => HomeBlocCubit(),
          child: HomeScreen(),
        );
      } else if (state is AuthBlocFailedState) {
        if (state.message != null || state.message != "") {
          Helper.showToast(context, state.message!);
        }
        return LoginPage();
      } else if (state is AuthBlocLoggedInState ||
          state is AuthBlocRegisterSuccessState) {
        if (state is AuthBlocRegisterSuccessState &&
            state.message != null &&
            state.message != "") {
          Helper.showToast(context, state.message!);
        }
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) => BlocProvider(
              create: (context) => HomeBlocCubit(),
              child: HomeScreen(),
            ),
          ),
        );
      }

      return Center(
          child: Text(kDebugMode ? "state not implemented $state" : ""));
    });
  }
}
