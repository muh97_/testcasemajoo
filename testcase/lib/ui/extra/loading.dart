import 'package:flutter/material.dart';

class LoadingIndicatorScreen extends StatelessWidget {
  final double height;

  const LoadingIndicatorScreen({Key? key, this.height = 10}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: CircularProgressIndicator(strokeWidth: 50),
      ),
    );
  }
}
