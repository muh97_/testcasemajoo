import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie_model.dart';

class MovieDetailsScreen extends StatefulWidget {
  final Results movie;

  MovieDetailsScreen({Key? key, required this.movie}) : super(key: key);

  @override
  State<MovieDetailsScreen> createState() => _MovieDetailsScreenState();
}

class _MovieDetailsScreenState extends State<MovieDetailsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        top: false,
        bottom: false,
        child: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                expandedHeight: 200.0,
                floating: false,
                pinned: true,
                elevation: 0.0,
                flexibleSpace: FlexibleSpaceBar(
                    background: Image.network(
                  "https://image.tmdb.org/t/p/w500${widget.movie.posterPath}",
                  fit: BoxFit.cover,
                )),
              ),
            ];
          },
          body: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Wrap(
                  direction: Axis.horizontal,
                  spacing: 50.0,
                  alignment: WrapAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text(
                        widget.movie.title ?? widget.movie.originalTitle ?? "-",
                        style: TextStyle(
                          fontSize: 25.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
                Container(margin: EdgeInsets.only(top: 8.0, bottom: 8.0)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    _buildMovieInfo(
                      "Vote Average",
                      "${widget.movie.voteAverage ?? '-'}",
                    ),
                    _buildMovieInfo(
                      "Release Date",
                      "${widget.movie.releaseDate ?? '-'}",
                    ),
                    _buildMovieInfo(
                      "Popularity",
                      "${widget.movie.popularity ?? '-'}",
                    ),
                  ],
                ),
                Container(margin: EdgeInsets.only(top: 8.0, bottom: 8.0)),
                Text(widget.movie.overview ?? "-"),
                Container(margin: EdgeInsets.only(top: 16.0, bottom: 16.0)),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildMovieInfo(String title, String value) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Text(
          title,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 15.0,
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 5.0),
        ),
        Text(
          value,
          style: TextStyle(
            fontSize: 15.0,
          ),
        ),
      ],
    );
  }
}
