import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie_model.dart';

import '../../common/route/enter_exit_route.dart';
import '../detail/detail_movie.dart';

class HomeLoadedScreen extends StatelessWidget {
  final List<Results> data;

  const HomeLoadedScreen({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Movies'),
        leading: Container(
          child: Icon(Icons.home),
        ),
      ),
      body: GridView.count(
        crossAxisCount: 2,
        crossAxisSpacing: 20,
        mainAxisSpacing: 10,
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        shrinkWrap: true,
        childAspectRatio: 150 / 250,
        physics: BouncingScrollPhysics(),
        children: data.map(
          (item) {
            return movieItemWidget(context, item);
          },
        ).toList(),
      ),
    );
  }

  Widget movieItemWidget(BuildContext context, Results item) {
    return GestureDetector(
      onTap: () {
        openShowDetails(context, item);
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Flexible(
            child: ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: AspectRatio(
                aspectRatio: 150 / 200,
                child: Image.network(
                  "https://image.tmdb.org/t/p/w500/" + item.posterPath!,
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          SizedBox(height: 6),
          Text(item.originalTitle ?? item.originalName ?? "-",
              style: TextStyle(fontSize: 12, color: Colors.black),
              maxLines: 1,
              overflow: TextOverflow.ellipsis),
          SizedBox(height: 2),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text('${item.releaseDate ?? ""}',
                  style: TextStyle(color: Colors.grey)),
              Container(
                  padding: EdgeInsets.symmetric(horizontal: 5, vertical: 3),
                  decoration: BoxDecoration(
                      color: Colors.blue,
                      borderRadius: BorderRadius.circular(10)),
                  child: Text('${item.voteAverage}',
                      style: TextStyle(color: Colors.white, fontSize: 11)))
            ],
          ),
        ],
      ),
    );
  }

  void openShowDetails(BuildContext context, Results item) {
    Navigator.push(
        context,
        EnterExitRoute(
          enterPage: MovieDetailsScreen(
            movie: item,
          ),
        ));
  }
}
