import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/home_bloc/home_bloc_cubit.dart';
import '../extra/error_screen.dart';
import '../extra/loading.dart';
import 'home_loaded_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  void initState() {
    networkConnectivityListener();
    super.initState();
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBlocCubit, HomeBlocState>(builder: (_, state) {
      if (state is HomeBlocLoadedState) {
        return HomeLoadedScreen(data: state.data);
      } else if (state is HomeBlocInitialState ||
          state is HomeBlocLoadingState) {
        return LoadingIndicatorScreen();
      } else if (state is HomeBlocErrorState) {
        return ErrorScreen(message: state.error);
      } else if (state is HomeBlocNetworkErrorState) {
        return ErrorScreen(
          message: "No internet connection",
          retryButton: Column(
            children: [
              SizedBox(
                height: 100,
              ),
              TextButton.icon(
                icon: Icon(
                  Icons.refresh_sharp,
                  color: Theme.of(context).colorScheme.primaryContainer,
                ),
                label: Text(
                  'Retry',
                  style: TextStyle(
                    color: Theme.of(context).colorScheme.primaryContainer,
                  ),
                ),
                onPressed: () {
                  networkConnectivityListener();
                },
              )
            ],
          ),
        );
      }

      return Center(
          child: Text(kDebugMode ? "state not implemented $state" : ""));
    });
  }

  void networkConnectivityListener() {
    _connectivitySubscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      context.read<HomeBlocCubit>().checkConnection(
          result == ConnectivityResult.mobile ||
              result == ConnectivityResult.wifi);
    });
  }
}
