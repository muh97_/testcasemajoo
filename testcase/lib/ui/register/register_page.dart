import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/register_bloc/register_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/models/user.dart';

import '../../bloc/home_bloc/home_bloc_cubit.dart';
import '../../utils/helper.dart';
import '../extra/loading.dart';
import '../home/home_screen.dart';

class RegisterPage extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<RegisterPage> {
  TextController _emailController = TextController();
  TextController _usernameController = TextController();
  TextController _passwordController = TextController();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Register Data'),
        ),
        body: BlocListener<RegisterBlocCubit, RegisterBlocState>(
          listener: (context, state) {
            if (state is RegisterBlocInitialState ||
                state is RegisterBlocLoadingState) {
              LoadingIndicatorScreen();
            } else if (state is RegisterBlocFailedState) {
              if (state.message != null || state.message != "") {
                Helper.showToast(context, state.message!);
              }
              RegisterPage();
            } else if (state is RegisterBlocSuccessState) {
              if (state.message != null && state.message != "") {
                Helper.showToast(context, state.message!);
              }
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => BlocProvider(
                    create: (context) => HomeBlocCubit(),
                    child: HomeScreen(),
                  ),
                ),
              );
            }
          },
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 10),
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: 9,
                  ),
                  _formRegister(),
                  SizedBox(
                    height: 50,
                  ),
                  CustomButton(
                    text: 'Register',
                    onPressed: handleRegister,
                    height: 100,
                  ),
                ],
              ),
            ),
          ),
        ));
  }

  Widget _formRegister() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _usernameController,
            isEmail: true,
            hint: 'majoo super apps,',
            label: 'Username',
            validator: (val) {
              return null;
            },
          ),
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
            validator: (val) {
              return null;
            },
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  void handleRegister() async {
    String _username = _usernameController.value;
    String _email = _emailController.value;
    String _password = _passwordController.value;

    if (_username == "" && _email == "" && _password == "") {
      Helper.showToast(context,
          'Form tidak boleh kosong,mohon cek kembali data yang anda inputkan');
      return;
    } else if (_email == "") {
      Helper.showToast(context, 'Email tidak boleh kosong');
      return;
    } else if (!Helper.isValidEmail(_email)) {
      Helper.showToast(context, 'Masukkan e-mail yang valid');
      return;
    } else if (_password == "") {
      Helper.showToast(context, 'Password tidak boleh kosong');
      return;
    } else if (!Helper.isValidEmail(_email) &&
        _password != "" &&
        _username == "") {
      Helper.showToast(context,
          'Form tidak boleh kosong,mohon cek kembali data yang anda inputkan');
      return;
    } else if (formKey.currentState!.validate()) {
      User user = User(
        userName: _username,
        email: _email,
        password: _password,
      );
      context.read<RegisterBlocCubit>().registeredUser(user);
    }
  }
}
