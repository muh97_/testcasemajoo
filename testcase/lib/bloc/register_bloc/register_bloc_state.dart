part of 'register_bloc_cubit.dart';

abstract class RegisterBlocState extends Equatable {
  RegisterBlocState() : super();

  @override
  List<Object> get props => [];
}

class RegisterBlocInitialState extends RegisterBlocState {}

class RegisterBlocLoadingState extends RegisterBlocState {}

class RegisterBlocFailedState extends RegisterBlocState {
  final String? message;

  RegisterBlocFailedState(this.message);

  @override
  List<Object> get props => [message ?? ""];
}

class RegisterBlocSuccessState extends RegisterBlocState {
  final String? message;

  RegisterBlocSuccessState(this.message);

  @override
  List<Object> get props => [message ?? ""];
}

class RegisterBlocLoadedState extends RegisterBlocState {
  final data;

  RegisterBlocLoadedState(this.data);

  @override
  List<Object> get props => [data];
}

class RegisterBlocErrorState extends RegisterBlocState {
  final error;

  RegisterBlocErrorState(this.error);

  @override
  List<Object> get props => [error];
}
