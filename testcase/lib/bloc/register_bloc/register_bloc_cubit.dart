import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../utils/database_helper.dart';

part 'register_bloc_state.dart';

class RegisterBlocCubit extends Cubit<RegisterBlocState> {
  RegisterBlocCubit() : super(RegisterBlocInitialState()) {
    emit(RegisterBlocInitialState());
  }
  
  void loginUser(User user) async {
    emit(RegisterBlocLoadingState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    DbHelper dbHelper = DbHelper();

    if (await dbHelper.login(user.email, user.password)) {
      await sharedPreferences.setBool("is_logged_in", true);
      String data = user.toJson().toString();
      sharedPreferences.setString("user_value", data);
        emit(RegisterBlocSuccessState("Register Berhasil"));
    } else {
      emit(RegisterBlocFailedState("Login gagal , periksa kembali inputan anda"));
    }
  }

  void registeredUser(User user) async {
    emit(RegisterBlocLoadingState());
    DbHelper dbHelper = DbHelper();
    bool isExist = await dbHelper.isExistRow(user) > 0;
    if (isExist) {
      emit(RegisterBlocFailedState("Username atau Email sudah digunakan"));
    } else {
      int result = await dbHelper.insertUser(user);
      if (result > 0) {
        loginUser(user);
      } else {
        emit(RegisterBlocErrorState("Gagal Menyimpan Data"));
      }
    }
  }
}
