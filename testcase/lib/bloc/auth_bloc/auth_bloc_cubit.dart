import 'package:bloc/bloc.dart';
import 'package:majootestcase/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../utils/database_helper.dart';
import 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState()) {
    emit(AuthBlocInitialState());
  }

  void fetchHistoryLogin() async {
    emit(AuthBlocInitialState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool isLoggedIn = sharedPreferences.getBool("is_logged_in") ?? false;
    if (isLoggedIn) {
      emit(AuthBlocLoggedInState());
    } else {
      emit(AuthBlocLoginState());
    }
  }

  void loginUser(User user) async {
    emit(AuthBlocLoadingState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    DbHelper dbHelper = DbHelper();

    if (await dbHelper.login(user.email, user.password)) {
      await sharedPreferences.setBool("is_logged_in", true);
      String data = user.toJson().toString();
      sharedPreferences.setString("user_value", data);
        emit(AuthBlocLoggedInState());
    } else {
      emit(AuthBlocFailedState("Login gagal , periksa kembali inputan anda"));
    }
  }
}
