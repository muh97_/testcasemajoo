import 'package:equatable/equatable.dart';

abstract class AuthBlocState extends Equatable {
  const AuthBlocState();

  @override
  List<Object> get props => [];
}

class AuthBlocInitialState extends AuthBlocState {}

class AuthBlocLoadingState extends AuthBlocState {}

class AuthBlocLoggedInState extends AuthBlocState {}

class AuthBlocLoginState extends AuthBlocState {}

class AuthBlocSuccesState extends AuthBlocState {}

class AuthBlocFailedState extends AuthBlocState {
  final String? message;

  AuthBlocFailedState(this.message);

  @override
  List<Object> get props => [message ?? ""];
}

class AuthBlocRegisterState extends AuthBlocState {
  final String? message;

  AuthBlocRegisterState([this.message]);

  @override
  List<Object> get props => [message ?? ""];
}

class AuthBlocRegisterSuccessState extends AuthBlocState {
  final String? message;

  AuthBlocRegisterSuccessState(this.message);

  @override
  List<Object> get props => [message ?? ""];
}

class AuthBlocLoadedState extends AuthBlocState {
  final data;

  AuthBlocLoadedState(this.data);

  @override
  List<Object> get props => [data];
}

class AuthBlocErrorState extends AuthBlocState {
  final error;

  AuthBlocErrorState(this.error);

  @override
  List<Object> get props => [error];
}
