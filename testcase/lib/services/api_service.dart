import 'package:dio/dio.dart';
import 'package:majootestcase/models/movie_model.dart';
import 'package:majootestcase/services/dio_config_service.dart' as dioConfig;

class ApiServices{

  Future<MovieModel?> getMovieList() async {
    try {
      var dio = await dioConfig.dio();
      Response response = await dio.get('trending/all/day');
      return MovieModel.fromJson(Map<String,dynamic>.from(response.data));
    } catch(e) {
      return null;
    }
  }
}